(def +version+ "0.0.2")

(set-env!
 :dependencies '[;; Compile dependencies
                 [org.clojure/tools.logging "0.5.0"]

                 ;; Test dependencies
                 [adzerk/boot-test "1.2.0" :scope "test"]
                 [org.clojure/core.async "0.4.500" :scope "test"]

                 ;; Provided dependencies
                 [adzerk/bootlaces "0.2.0" :scope "provided"]
                 [ch.qos.logback/logback-classic "1.2.3" :scope "provided"]
                 [org.clojure/clojure "1.10.1" :scope "provided"]
                 [org.slf4j/slf4j-api "1.7.28" :scope "provided"]]
 :source-paths #{"src"})

(require '[adzerk.bootlaces :as bl]
         '[adzerk.boot-test :as bt]
         '[clojure.string :as str])

(def is-snapshot? (str/ends-with? +version+ "-SNAPSHOT"))

(bl/bootlaces! +version+)

(task-options!
 ;; bootlaces! overrides `push` options. Make sure to override them back.
 pom {:project (if is-snapshot? 'org.clojars.gmeb/timer 'gmeb/timer)
      :version +version+
      :description "A Clojure-native library for managing timers."
      :url "http://gitlab.com/gmeb/timer"
      :scm {:url "http://gitlab.com/gmeb/timer"}
      :developers {"Arnaud Installe" "installe@gmail.com"}
      :license {"BSD License"
                "https://choosealicense.com/licenses/bsd-3-clause/"}})

(deftask set-test-paths
  []
  (merge-env! :source-paths #{"test"})
  identity)

(deftask git-push-tag
  []
  (println "Pushing tag to git.")
  (sh "git" "push" "--tags")
  identity)

(deftask test
  "Run all tests."
  []
  (comp (set-test-paths) (bt/test)))

(deftask build
  "Create a build and install in the local maven repository."
  []
  (comp (test) (bl/build-jar)))

(deftask deploy
  "Create a build, and push it to clojars."
  []
  ;; Taken from bl/push-snapshot, since we must override :ensure-branch.
  (comp (build)
        (#'bl/collect-clojars-credentials)
        (if is-snapshot?
          (push :ensure-branch nil
                :ensure-snapshot true)
          (comp (push :tag true
                      :ensure-branch nil
                      :ensure-clean true
                      :ensure-release true)
                (git-push-tag)))))
