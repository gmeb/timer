# gmeb/timer [![build][badge]][commits]

A Clojure-native library for managing timers.

## Dependency

[](dependency)
```clojure
[gmeb/timer "0.0.2"] ;; latest release
```
[](/dependency)

## Usage

The latest API documentation can be found at [doc].

## References

* Repository: [repo]
* Clojars: [clojars]
* Documentation: [doc]

## License

Copyright © 2019 Arnaud Installe

Licensed under the [BSD License][license].

[badge]: https://gitlab.com/gmeb/timer/badges/master/pipeline.svg
[clojars]: https://clojars.org/gmeb/timer
[commits]: https://gitlab.com/gmeb/timer/commits/master
[doc]: https://cljdoc.org/d/gmeb/timer/0.0.1
[license]: https://gitlab.com/gmeb/timer/blob/master/LICENSE
[repo]: https://gitlab.com/gmeb/timer
