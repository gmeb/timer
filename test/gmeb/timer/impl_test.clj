(ns gmeb.timer.impl-test
  (:require [clojure.core.async :refer [<!! >!! chan close!]]
            [clojure.test :refer :all]
            [clojure.tools.logging :as log]
            [gmeb.timer.impl :as impl]))

(defn timer
  [id]
  (let [fun (fn [id] (log/info "Running timer" id))]
    (partial fun id)))

(deftest test-start-timer-followed-by-stop-timer
  (let [echo-chan (chan)
        timer-fun (fn[] (>!! echo-chan "ping"))]
    (impl/start-timer :foo timer-fun 1000)
    (is (impl/has-timer :foo))
    (is (= (<!! echo-chan) "ping"))
    (impl/stop-timer :foo)
    (is (not (impl/has-timer :foo)))
    (close! echo-chan)))

(deftest start-timer-should-not-allow-starting-timer-with-existing-id
  (impl/start-timer :foo (timer 1) 1000)
  (is (try (do
             (impl/start-timer :foo (timer 2) 100)
             false)
           (catch Throwable t true)))
  (impl/stop-timer :foo))

(deftest stop-all-timers-should-stop-all-timers
  (impl/start-timer :foo (timer :foo) 1000)
  (impl/start-timer :bar (timer :bar) 1000)
  (is (and (impl/has-timer :foo)
           (impl/has-timer :bar)
           (not (impl/has-timer :baz))))
  (impl/stop-all-timers)
  (is (and (map #(-> % impl/has-timer not) [:foo :bar :baz]))))
