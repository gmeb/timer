(ns gmeb.timer
  "External interface for starting and stopping timers."
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as st]
            [gmeb.timer.impl :as impl]))

(s/fdef start-timer
        :args (s/cat :id keyword?
                     :fun fn?
                     :interval-in-ms integer?)
        :ret keyword?
        :fn #(= (:ret %) (-> % :args :id)))
(defn start-timer
  "Starts a timer with id `id` that runs function `fun` every `ms` milliseconds."
  [id fun interval-in-ms]
  (impl/start-timer id fun interval-in-ms))

(s/fdef stop-timer
        :args (s/cat :id keyword?)
        :ret nil)
(defn stop-timer
  "Stops the timer with id `id`."
  [id]
  (impl/stop-timer id))

(defn stop-all-timers
  "Stops all timers."
  []
  (impl/stop-all-timers))

(st/instrument)
