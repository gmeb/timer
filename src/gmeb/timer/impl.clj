(ns gmeb.timer.impl
  "Implementation of `gmeb.timer` interface."
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as st]
            [clojure.tools.logging :as log]))

(defrecord Metadata [id fun interval-in-ms])

(def ^:private initial-state
     "Initial state of the module.
     `:timers` maps ids to timer metadata."
     {:timers {}})

(defonce ^:private state (atom initial-state))

(defn has-timer
  "Evaluates to true if the timer with `id` has been registered."
  [id]
  (-> @state :timers id))

(defn- sleep
  [interval-in-ms]
  (Thread/sleep interval-in-ms))

(defn- create-thread
  [metadata]
  (future
   (loop []
     (if (-> metadata :id has-timer)
       (do (log/trace "Starting timer" (:id metadata) "...")
           (future ((:fun metadata)))
           (sleep (:interval-in-ms metadata))
           (log/trace "Timer" (:id metadata) "finished.")
           (recur))))))

(defn start-timer
  "See `gmeb.timer/start-timer`."
  [id fun interval-in-ms]
  (if (has-timer id)
    (throw (Exception. (str "A timer with" id "already exists."))))
  (let [metadata (Metadata. id fun interval-in-ms)]
    (swap! state update :timers assoc id metadata)
    (create-thread metadata)
    (log/debug "Timer" id "created with interval" interval-in-ms "ms.")
    id))

(defn stop-timer
  "See `gmeb.timer/stop-timer`."
  [id]
  (swap! state update :timers dissoc id)
  (log/debug "Timer" id "stopped."))

(defn stop-all-timers
  []
  "See `gmeb.timer/stop-all-timers`."
  (compare-and-set! state @state initial-state))